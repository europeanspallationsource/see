/*
 * Copyright (C) 2012 Ibigo d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ibigo.core.db;

import com.google.common.base.Preconditions;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import java.io.InputStream;
import java.sql.Blob;

/**
 * @author Marko Kolar <marko@ibigo.com>
 */
public class JpaPersistenceManager implements PersistenceManager {

    private final EntityManager entityManager;

    public JpaPersistenceManager(EntityManager entityManager) {
        Preconditions.checkNotNull(entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public Blob createBlob(byte[] content) {
        final Session session = (Session) entityManager.getDelegate();
        return session.getLobHelper().createBlob(content);
    }

    @Override
    public Blob createBlob(InputStream inputStream, long length) {
        final Session session = (Session) entityManager.getDelegate();
        return session.getLobHelper().createBlob(inputStream, length);
    }

    @Override
    public JPQLQuery select() {
        return new JPAQuery(entityManager);
    }

    @Override
    public void persist(Persistable entity) {
        entityManager.persist(entity);
    }
    
    @Override
    public void remove(Persistable entity) {
        entityManager.remove(entity);
    }

    @Override
    public <T extends Persistable> T managed(T entity) {
        return (T) entityManager.find(entity.getClass(), entity.jpaId());
    }
}
