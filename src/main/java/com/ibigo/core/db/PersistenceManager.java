/*
 * Copyright (C) 2012 Ibigo d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ibigo.core.db;

import com.mysema.query.jpa.JPQLQuery;

import java.io.InputStream;
import java.sql.Blob;

/**
 * @author Marko Kolar <marko@ibigo.com>
 */
public interface PersistenceManager {

    Blob createBlob(byte[] content);

    Blob createBlob(InputStream inputStream, long length);

    JPQLQuery select();

    void persist(Persistable entity);

    void remove(Persistable entity);

    <T extends Persistable> T managed(T entity);
}
