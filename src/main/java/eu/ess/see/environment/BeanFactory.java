package eu.ess.ccdb.environment;

import com.ibigo.core.db.JpaPersistenceManager;
import com.ibigo.core.db.PersistenceManager;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Stateless
public class BeanFactory {
    @PersistenceContext private EntityManager em;

    @Produces public PersistenceManager persistenceManager() {
        return new JpaPersistenceManager(em);
    }
}
