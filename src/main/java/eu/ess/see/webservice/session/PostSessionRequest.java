package eu.ess.see.webservice.session;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
class PostSessionRequest {

    private NotebookData notebook;

    @JsonCreator
    public PostSessionRequest(@JsonProperty("notebook") NotebookData notebook) {
        this.notebook = notebook;
    }

    public NotebookData getNotebook() { return notebook; }

}
