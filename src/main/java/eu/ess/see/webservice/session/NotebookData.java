package eu.ess.see.webservice.session;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
* @author Marko Kolar <marko.kolar@cosylab.com>
*/
class NotebookData {
    private String path;
    private String name;

    @JsonCreator
    public NotebookData(@JsonProperty("path") String path, @JsonProperty("name") String name) {
        this.path = path;
        this.name = name;
    }

    public String getPath() { return path; }
    public String getName() { return name; }
}
