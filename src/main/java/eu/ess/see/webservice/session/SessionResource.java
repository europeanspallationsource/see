package eu.ess.see.webservice.session;

import eu.ess.see.model.Notebook;
import eu.ess.see.services.Kernel;
import eu.ess.see.services.KernelService;
import eu.ess.see.services.NotebookService;
import eu.ess.see.util.As;
import eu.ess.see.webservice.filters.OverrideContentTypeAsJson;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Path("/sessions")
public class SessionResource {

    @Inject private NotebookService notebookService;
    @Inject private KernelService kernelService;

    @POST
    @OverrideContentTypeAsJson
    @Produces(MediaType.APPLICATION_JSON)
    public PostSessionResponse post(PostSessionRequest request) {
        final Notebook notebook = As.notNull(notebookService.notebookWithName(request.getNotebook().getName()));
        final Kernel kernel = kernelService.kernel(notebook);
        return new PostSessionResponse(kernel.uuid().toString(), new NotebookData("", notebook.name()), new KernelData(kernel.uuid().toString()));
    }
}
