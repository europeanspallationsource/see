package eu.ess.see.webservice.session;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
* @author Marko Kolar <marko.kolar@cosylab.com>
*/
class KernelData {
    private String id;

    @JsonCreator
    public KernelData(@JsonProperty("id") String id) {
        this.id = id;
    }

    public String getId() { return id; }
}
