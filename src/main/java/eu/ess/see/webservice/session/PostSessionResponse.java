package eu.ess.see.webservice.session;

import org.codehaus.jackson.annotate.JsonCreator;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
class PostSessionResponse {

    private String id;
    private NotebookData notebook;
    private KernelData kernel;

    @JsonCreator
    public PostSessionResponse(String id, NotebookData notebook, KernelData kernel) {
        this.id = id;
        this.notebook = notebook;
        this.kernel = kernel;
    }

    public String id() { return id; }
    public NotebookData getNotebook() { return notebook; }
    public KernelData getKernel() { return kernel; }
}
