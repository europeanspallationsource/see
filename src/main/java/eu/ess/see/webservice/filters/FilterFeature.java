package eu.ess.see.webservice.filters;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Provider
public class FilterFeature implements DynamicFeature {

    @Override public void configure(ResourceInfo resourceInfo, FeatureContext featureContext) {
        if (resourceInfo.getResourceMethod().isAnnotationPresent(OverrideContentTypeAsJson.class)) {
            featureContext.register(OverrideContentTypeAsJsonFilter.class);
        }
    }
}
