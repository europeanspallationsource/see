package eu.ess.see.webservice.filters;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import java.io.IOException;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Priority(Priorities.HEADER_DECORATOR)
public class OverrideContentTypeAsJsonFilter implements ContainerRequestFilter {

    @Override public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        containerRequestContext.getHeaders().putSingle("Content-Type", "application/json");
    }
}
