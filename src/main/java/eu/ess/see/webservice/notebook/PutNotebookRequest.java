package eu.ess.see.webservice.notebook;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.node.ObjectNode;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class PutNotebookRequest {

    private ObjectNode content;

    @JsonCreator
    public PutNotebookRequest(@JsonProperty("content") ObjectNode content) {
        this.content = content;
    }

    public ObjectNode getContent() { return content; }
}
