package eu.ess.see.webservice.notebook;

import eu.ess.see.model.Notebook;
import eu.ess.see.model.NotebookRevision;
import eu.ess.see.services.NotebookService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Path("/notebooks/{notebookId}")
public class NotebookResource {

    @Inject private NotebookService notebookService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetNotebookResponse get(@PathParam("notebookId") String notebookId) {
        final @Nullable Notebook notebook = notebookService.notebookWithName(notebookId);
        final NotebookRevision notebookRevision = notebookService.currentRevision(notebook);

        final ObjectMapper objectMapper = new ObjectMapper();
        final ObjectNode contentNode;
        try {
            contentNode = (ObjectNode) objectMapper.readTree(notebookRevision.content());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new GetNotebookResponse(notebookId, notebookRevision.creationDate().toString(), contentNode);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void put(@PathParam("notebookId") String notebookId, PutNotebookRequest data) {
        notebookService.saveNotebookRevision(notebookId, data.getContent().toString());
    }

    @DELETE
    public void delete(@PathParam("notebookId") String notebookId) {
        System.out.println("Deleting notebook.");
    }

    @GET
    @Path("/checkpoints")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCheckpoints(@PathParam("notebookId") String notebookId) {
        return "{}";
    }

    @POST
    @Path("/checkpoints")
    public void postCheckpoints(@PathParam("notebookId") String notebookId) {
    }
}
