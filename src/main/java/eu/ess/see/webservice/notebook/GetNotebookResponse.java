package eu.ess.see.webservice.notebook;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.node.ObjectNode;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
class GetNotebookResponse {

    private String name;
    private String created;
    private ObjectNode content;

    @JsonCreator
    public GetNotebookResponse(String name, String created, ObjectNode content) {
        this.name = name;
        this.created = created;
        this.content = content;
    }

    public String getName() { return name; }
    public String getCreated() { return created; }
    public ObjectNode getContent() { return content; }
}
