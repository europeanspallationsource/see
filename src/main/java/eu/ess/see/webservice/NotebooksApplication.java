package eu.ess.see.webservice;

import com.google.common.collect.ImmutableSet;
import eu.ess.see.webservice.filters.FilterFeature;
import eu.ess.see.webservice.notebook.NotebookResource;
import eu.ess.see.webservice.session.SessionResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@ApplicationPath("/api")
public class NotebooksApplication extends Application {

    @Override public Set<Class<?>> getClasses() {
        return ImmutableSet.<Class<?>>of(FilterFeature.class, NotebookResource.class, SessionResource.class);
    }

}
