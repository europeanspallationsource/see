package eu.ess.see.webservice.kernel;

import eu.ess.see.services.Kernel;
import eu.ess.see.services.KernelService;
import eu.ess.see.services.NotebookService;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.UUID;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@ServerEndpoint("/api/kernels/{kernelUuid}/iopub")
public class IopubKernelEndpoint {

    @Inject private NotebookService notebookService;
    @Inject private KernelService kernelService;

    @OnMessage
    public @Nullable String onMessage(String message, @PathParam("kernelUuid") String kernelUuid) {
        return null;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("kernelUuid") String kernelUuid) {
       final Kernel kernel = kernelService.kernel(UUID.fromString(kernelUuid));
        kernel.subscribeIopubSession(session);
    }

    @OnClose
    public void onClose(Session session, CloseReason reason, @PathParam("kernelUuid") String kernelUuid) {
        final Kernel kernel = kernelService.kernel(UUID.fromString(kernelUuid));
        kernel.unsubscribeIopubSession(session);
    }
}
