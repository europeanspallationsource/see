package eu.ess.see.webservice.kernel;

import eu.ess.see.services.KernelService;
import eu.ess.see.services.NotebookService;

import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.UUID;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@ServerEndpoint("/api/kernels/{kernelUuid}/shell")
public class ShellKernelEndpoint {

    @Inject private NotebookService notebookService;
    @Inject private KernelService kernelService;

    @OnMessage
    public String onMessage(String message, @PathParam("kernelUuid") String kernelUuid) {
        if (message.contains("header")) {
            return kernelService.kernel(UUID.fromString(kernelUuid)).executeCommand(message);
        } else {
            // TODO: Handle session message
            return null;
        }
    }

    @OnOpen
    public void onOpen(Session session) {
    }

    @OnClose
    public void onClose(CloseReason reason) {
    }
}
