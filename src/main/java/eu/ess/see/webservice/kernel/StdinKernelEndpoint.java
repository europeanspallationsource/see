package eu.ess.see.webservice.kernel;

import eu.ess.see.services.NotebookService;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@ServerEndpoint("/api/kernels/{kernelUuid}/stdin")
public class StdinKernelEndpoint {

    @Inject private NotebookService notebookService;

    @OnMessage
    public @Nullable String onMessage(String message) {
        return null;
    }

    @OnOpen
    public void onOpen(Session session) {
    }

    @OnClose
    public void onClose(CloseReason reason) {
    }
}
