package eu.ess.see.services;

import com.google.common.collect.Maps;
import eu.ess.see.model.Notebook;

import javax.annotation.Nullable;
import javax.ejb.Singleton;
import java.util.Map;
import java.util.UUID;

/**
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Singleton
public class KernelService {

    private Map<Notebook, Kernel> kernelByNotebook = Maps.newHashMap();
    private Map<UUID, Kernel> kernelByKernelUUID = Maps.newHashMap();

    public synchronized Kernel kernel(Notebook notebook) {
        final @Nullable Kernel existingKernel = kernelByNotebook.get(notebook);
        if (existingKernel != null) {
            return existingKernel;
        } else {
            final Kernel newKernel = Kernel.startNew();
            kernelByNotebook.put(notebook, newKernel);
            kernelByKernelUUID.put(newKernel.uuid(), newKernel);
            return newKernel;
        }
    }

    public synchronized Kernel kernel(UUID uuid) { return kernelByKernelUUID.get(uuid); }
}
