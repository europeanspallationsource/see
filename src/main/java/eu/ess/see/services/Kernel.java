package eu.ess.see.services;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.apache.commons.exec.*;
import org.apache.commons.exec.environment.EnvironmentUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.zeromq.ZMQ;

import javax.websocket.Session;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
public class Kernel {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static Kernel startNew() {
        final Kernel kernel = new Kernel();
        kernel.start();
        return kernel;
    }

    private UUID uuid;

    private int shellPort;
    private int iopubPort;
    private int stdinPort;

    private ExecuteWatchdog watchdog;

    private Set<Session> sessions = Sets.newHashSet();

    private Kernel() {}

    public UUID uuid() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
        return uuid;
    }

    public String executeCommand(String command) {
        try (
            final ZMQ.Context context = ZMQ.context(1);
            final ZMQ.Socket shell = context.socket(ZMQ.DEALER);
        ) {
            shell.connect("tcp://localhost:" + shellPort);
            webToZmq(command, shell);
            return zmqToWeb(shell);
        }
    }

    public void subscribeIopubSession(Session session) {
        sessions.add(session);
    }

    public void unsubscribeIopubSession(Session session) {
        sessions.remove(session);
    }

    @Override public boolean equals(Object other) {
        if (other instanceof Kernel) {
            return Objects.equal(((Kernel) other).uuid(), uuid());
        } else {
            return false;
        }
    }

    @Override public int hashCode() {
        return Objects.hashCode(uuid());
    }

    private void start() {
        shellPort = getRandomFreePort();
        iopubPort = getRandomFreePort();
        stdinPort = getRandomFreePort();


        final DefaultExecutor executor = new DefaultExecutor();
        watchdog = new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT);
        executor.setWatchdog(watchdog);

        try {
            final CommandLine cmdLine = CommandLine.parse("ipython kernel --shell=" + shellPort + " --iopub=" + iopubPort + " --stdin=" + stdinPort + " --no-secure");
            executor.execute(cmdLine, EnvironmentUtils.getProcEnvironment(), new DefaultExecuteResultHandler() {
                @Override public void onProcessFailed(ExecuteException e) {
                    e.printStackTrace();
                }
            });
            Thread.sleep(100); // TODO: Figure out why this is needed and solve it in a cleaner way.
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        final Thread listeningThread = new Thread(new IopubRunnable());
        listeningThread.start();
    }

    private int getRandomFreePort() {
        try (final ServerSocket server = new ServerSocket(0)) {
            return server.getLocalPort();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String zmqToWeb(ZMQ.Socket socket) {
        try {
            while (!socket.recvStr().equals("<IDS|MSG>")) {
                // Do nothing.
            }

            final String hmacString = socket.recvStr();
            final String headerString = socket.recvStr();
            final String parentHeaderString = socket.recvStr();
            final String metadataHeaderString = socket.recvStr();
            final String contentHeaderString = socket.recvStr();

            final JsonNode header = objectMapper.readTree(headerString);
            final ObjectNode responseObject = objectMapper.createObjectNode();
            responseObject.put("header", header);
            responseObject.put("msg_type", header.get("msg_type"));
            responseObject.put("msg_id", header.get("msg_id"));
            responseObject.put("parent_header", objectMapper.readTree(parentHeaderString));
            responseObject.put("metadata", objectMapper.readTree(metadataHeaderString));
            responseObject.put("content", objectMapper.readTree(contentHeaderString));

            while (socket.hasReceiveMore()) {
                socket.recv();
            }

            return responseObject.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void webToZmq(String command, ZMQ.Socket socket) {
        try {
            final JsonNode commandObject = objectMapper.readTree(command);
            socket.send("<IDS|MSG>", ZMQ.SNDMORE);
            socket.send("", ZMQ.SNDMORE);
            socket.send(commandObject.get("header").toString(), ZMQ.SNDMORE);
            socket.send(commandObject.get("parent_header").toString(), ZMQ.SNDMORE);
            socket.send(commandObject.get("metadata").toString(), ZMQ.SNDMORE);
            socket.send(commandObject.get("content").toString(), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private class IopubRunnable implements Runnable {
        @Override public void run() {
            final ZMQ.Context context = ZMQ.context(1);
            final ZMQ.Socket iopubSocket = context.socket(ZMQ.SUB);

            iopubSocket.connect("tcp://localhost:" + iopubPort);
            iopubSocket.subscribe(ZMQ.SUBSCRIPTION_ALL);

            while (!Thread.currentThread().isInterrupted ()) {
                final String response = zmqToWeb(iopubSocket);
                for (Session session : sessions) {
                    session.getAsyncRemote().sendText(response);
                }
            }
        }
    }
}
