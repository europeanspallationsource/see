package eu.ess.see.services;

import com.ibigo.core.db.PersistenceManager;
import com.mysema.query.alias.Alias;
import eu.ess.see.model.Notebook;
import eu.ess.see.model.NotebookRevision;
import eu.ess.see.util.As;


import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.inject.Inject;

import java.util.Date;
import java.util.UUID;

import static com.mysema.query.alias.Alias.$;


/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Stateless
public class NotebookService {

    @Inject private PersistenceManager pm;

    public @Nullable Notebook notebookWithUuid(UUID uuid) {
        final Notebook Notebook = Alias.alias(Notebook.class);
        return pm.select().from($(Notebook)).where($(Notebook.uuid()).eq(uuid)).uniqueResult($(Notebook));
    }

    public @Nullable Notebook notebookWithName(String name) {
        final Notebook Notebook = Alias.alias(Notebook.class);
        return pm.select().from($(Notebook)).where($(Notebook.name()).eq(name)).uniqueResult($(Notebook));
    }

    public NotebookRevision currentRevision(Notebook notebook) {
        final NotebookRevision NotebookRevision = Alias.alias(NotebookRevision.class);
        return As.notNull(pm.select().from($(NotebookRevision)).where($(NotebookRevision.notebook()).eq(notebook)).orderBy($(NotebookRevision.jpaId()).desc()).limit(1).uniqueResult($(NotebookRevision)));
    }

    public Notebook newNotebook(String name) {
        final Notebook notebook = new Notebook(name);
        final NotebookRevision notebookRevision = new NotebookRevision(notebook, new Date(), "{\"nbformat\": 3, \"worksheets\": [], \"metadata\": {\"name\": \"\", \"signature\": \"sha256:cd605fe1c34cdc50620ccdd0f629195829dbdab1722bb5dbeb4ee01a7b6c3904\"}, \"nbformat_minor\": 0}");
        pm.persist(notebook);
        pm.persist(notebookRevision);
        return notebook;
    }

    public void saveNotebookRevision(String name, String content) {
        final Notebook notebook = As.notNull(notebookWithName(name));
        pm.persist(new NotebookRevision(notebook, new Date(), content));
    }
}
