package eu.ess.see.model;

import com.ibigo.core.db.Persistable;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Entity
public class NotebookRevision extends Persistable {

    private @ManyToOne Notebook notebook;
    private Date creationDate;
    private @Type(type="org.hibernate.type.StringClobType") String content;

    protected NotebookRevision() {}

    public NotebookRevision(Notebook notebook, Date creationDate, String content) {
        this.notebook = notebook;
        this.creationDate = creationDate;
        this.content = content;
    }

    public Notebook notebook() { return notebook; }
    public Date creationDate() { return creationDate; }
    public String content() { return content; }
}
