package eu.ess.see.model;

import com.google.common.base.Objects;
import com.ibigo.core.db.Persistable;

import javax.persistence.Entity;
import java.util.UUID;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Entity
public class Notebook extends Persistable {

    private UUID uuid;

    private String name;

    protected Notebook() {}

    public Notebook(String name) {
        this.name = name;
    }

    public UUID uuid() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
        return uuid;
    }

    public String name() { return name; }

    @Override public boolean equals(Object other) {
        if (other instanceof Notebook) {
            return Objects.equal(((Notebook) other).uuid(), uuid());
        } else {
            return false;
        }
    }

    @Override public int hashCode() {
        return Objects.hashCode(uuid());
    }
}
