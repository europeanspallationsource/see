package eu.ess.see.model;

import com.ibigo.core.db.Persistable;
import javax.persistence.Entity;

/**
 *
 * @author Marko Kolar <marko.kolar@cosylab.com>
 */
@Entity
public class AppInfo extends Persistable {
}
